<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

$router->group(['prefix' => 'master', 'middleware' => 'auth'], function () use ($router) {

  $router->get('/department', 'MasterController@departmentList');
  $router->get('/department/create', 'MasterController@departmentCreate');
  $router->post('/department/insert', 'MasterController@departmentInsert');
  $router->get('/department/edit', 'MasterController@departmentEdit');
  $router->post('/department/update', 'MasterController@departmentUpdate');
  $router->get('/department/delete', 'MasterController@departmentDelete');

  $router->get('/golongan', 'MasterController@golonganList');
  $router->get('/golongan/create', 'MasterController@golonganCreate');
  $router->post('/golongan/insert', 'MasterController@golonganInsert');
  $router->get('/golongan/edit', 'MasterController@golonganEdit');
  $router->post('/golongan/update', 'MasterController@golonganUpdate');
  $router->get('/golongan/delete', 'MasterController@golonganDelete');

  $router->get('/subGolongan', 'MasterController@subGolonganList');
  $router->get('/subGolongan/create', 'MasterController@subGolonganCreate');
  $router->post('/subGolongan/insert', 'MasterController@subGolonganInsert');
  $router->get('/subGolongan/edit', 'MasterController@subGolonganEdit');
  $router->post('/subGolongan/update', 'MasterController@subGolonganUpdate');
  $router->get('/subGolongan/delete', 'MasterController@subGolonganDelete');

  $router->get('/lokasi', 'MasterController@lokasiList');
  $router->post('/lokasi/create', 'MasterController@lokasiCreate');
  $router->post('/lokasi/edit', 'MasterController@lokasiEdit');
  $router->post('/lokasi/update', 'MasterController@lokasiUpdate');
  $router->post('/lokasi/delete', 'MasterController@lokasiDelete');

  $router->get('/pic', 'MasterController@picList');
  $router->post('/pic/create', 'MasterController@picCreate');
  $router->post('/pic/edit', 'MasterController@picEdit');
  $router->post('/pic/update', 'MasterController@picUpdate');
  $router->post('/pic/delete', 'MasterController@picDelete');

  $router->get('/supplier', 'MasterController@supplierList');
  $router->post('/supplier/create', 'MasterController@supplierCreate');
  $router->post('/supplier/edit', 'MasterController@supplierEdit');
  $router->post('/supplier/update', 'MasterController@supplierUpdate');
  $router->post('/supplier/delete', 'MasterController@supplierDelete');

  $router->get('/tipeAset', 'MasterController@tipeAsetList');
  $router->post('/tipeAset/create', 'MasterController@tipeAsetCreate');
  $router->post('/tipeAset/edit', 'MasterController@tipeAsetEdit');
  $router->post('/tipeAset/update', 'MasterController@tipeAsetUpdate');
  $router->post('/tipeAset/delete', 'MasterController@tipeAsetDelete');
});

$router->group(['prefix' => 'aset', 'middleware' => 'auth'], function () use ($router) {
  $router->get('/list', 'AsetController@list');
  $router->get('/create', 'AsetController@create');
  $router->post('/insert', 'AsetController@insert');
  $router->get('/edit', 'AsetController@edit');
  $router->post('/update', 'AsetController@update');
  $router->post('/delete', 'AsetController@delete');
  $router->get('/detail', 'AsetController@detail');

  $router->get('/pemusnahan_aset', 'AsetController@pemusnahanAset');

  $router->get('/penurunan_nilai_aset', 'AsetController@penurunanNilaiAset');
  $router->get('/pemusnahan_aset', 'AsetController@pemusnahanAset');

  $router->get('/perbaikan_aset', 'AsetController@perbaikanAset');
  $router->get('/aset_rusak', 'AsetController@asetRusak');

  $router->get('/pindah_lokasi_aset', 'AsetController@pindahLokasiAset');
  $router->post('/pindah_aset', 'AsetController@pindahAset');
  $router->get('/penjualan_aset', 'AsetController@penjualanAset');
  $router->post('/jual_aset', 'AsetController@jualAset');

  $router->get('/data_aset_terjual', 'AsetController@dataAsetTerjual');
  $router->get('/data_aset_rusak', 'AsetController@dataAsetRusak');
  $router->get('/data_aset_berdasarkan_kategori', 'AsetController@dataAsetBerdasarkanKategori');
  $router->get('/data_aset_termusnahkan', 'AsetController@dataAsetTermusnahkan');
  $router->get('/lokasi_aset', 'AsetController@lokasiAset');
});

$router->group(['prefix' => 'user', 'middleware' => 'auth'], function () use ($router) {
  $router->get('/list', 'MasterController@userList');
  $router->get('/create', 'MasterController@createUser');
  $router->post('/insert', 'MasterController@insertUser');
  $router->get('/edit', 'MasterController@editUser');
  $router->post('/update', 'MasterController@updateUser');
  $router->get('/delete', 'MasterController@deleteUser');
});
