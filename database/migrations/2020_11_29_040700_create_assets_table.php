<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('kode_aset');
            $table->string('kode_dept');
            $table->string('kode_golongan');
            $table->string('kode_subgolongan');
            $table->string('nama_aset');
            $table->string('tipe_aset');
            $table->string('pendataan_ke');
            $table->string('tahun_pembelian');
            $table->string('merek');
            $table->string('tipe');
            $table->string('ukuran');
            $table->string('harga');
            $table->string('jumlah');
            $table->text('spesifikasi')->nullable();
            $table->string('tahun_berjalan');
            $table->string('perkiraan_umur_ekonomis');
            $table->string('identitas_komputer')->nullable();
            $table->string('no_mesin')->nullable();
            $table->string('no_rangka')->nullable();
            $table->string('warna')->nullable();
            $table->string('no_bpkb')->nullable();
            $table->string('no_polisi')->nullable();
            $table->string('kepemilikan_atas_nama')->nullable();
            $table->string('model_pembuatan')->nullable();
            $table->string('pic')->nullable();
            $table->string('supplier')->nullable();
            $table->string('fungsi')->nullable();
            $table->string('dic');
            $table->string('lokasi');
            $table->string('foto');
            $table->string('status');
            $table->string('barcode');
            $table->string('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
