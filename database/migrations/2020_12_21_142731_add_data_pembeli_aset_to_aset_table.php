<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataPembeliAsetToAsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
          $table->string('nama_pembeli')->nullable();
          $table->string('perusahaan_pembeli')->nullable();
          $table->string('alamat_pembeli')->nullable();
          $table->string('telp_pembeli')->nullable();
          $table->string('nilai_jual')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
          $table->dropColumn('nama_pembeli');
          $table->dropColumn('perusahaan_pembeli');
          $table->dropColumn('alamat_pembeli');
          $table->dropColumn('telp_pembeli');
          $table->dropColumn('nilai_jual');
        });
    }
}
