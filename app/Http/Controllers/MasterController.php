<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Golongan;
use App\SubGolongan;
use App\Lokasi;
use App\Pic;
use App\Supplier;
use App\TipeAset;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class MasterController extends Controller
{

  public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->jabatan = Auth::user()->jabatan;

            return $next($request);
        });
    }

    public function departmentList(Request $request)
    {
      $departments = Department::where('active', 'true')->get();

      $data = [
        'departments' => $departments
      ];

      return view('master.department.index', $data);
    }

    public function departmentCreate(Request $request)
    {
      return view('master.department.create');
    }

    public function departmentInsert(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'initial' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $department = Department::create([
        'kode' => $request->input('kode'),
        'initial' => $request->input('initial'),
        'nama' => $request->input('nama'),
        'active' => 'true'
      ]);

      return redirect()->back()->with('message', 'Sukses Input Data!');
    }

    public function departmentEdit(Request $request)
    {
      $department = Department::find($request->id);

      $data = [
        'department' => $department,
      ];

      return view('master.department.edit', $data);
    }

    public function departmentUpdate(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'initial' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $department = Department::find($request->id);
      $department->kode = $request->input('kode');
      $department->initial = $request->input('initial');
      $department->nama = $request->input('nama');
      $department->save();

      return redirect()->back()->with('message', 'Sukses Update Data!');
    }

    public function departmentDelete(Request $request)
    {
      $department = Department::find($request->id);
      $department->active = 'false';
      $department->save();

      return redirect()->back()->with('message', 'Sukses Hapus Data!');
    }

    public function golonganList(Request $request)
    {
      $golongans = Golongan::where('active', 'true')->get();

      $data = [
        'golongans' => $golongans
      ];

      return view('master.golongan.index', $data);
    }

    public function golonganCreate(Request $request)
    {
      return view('master.golongan.create');
    }

    public function golonganInsert(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $golongan = Golongan::create([
        'kode' => $request->input('kode'),
        'nama' => $request->input('nama'),
        'active' => 'true'
      ]);

      return redirect()->back()->with('message', 'Sukses Input Data!');
    }

    public function golonganEdit(Request $request)
    {
      $golongan = Golongan::find($request->id);

      $data = [
        'golongan' => $golongan,
      ];

      return view('master.golongan.edit', $data);
    }

    public function golonganUpdate(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $golongan = Golongan::find($request->id);
      $golongan->kode = $request->input('kode');
      $golongan->nama = $request->input('nama');
      $golongan->save();

      return redirect()->back()->with('message', 'Sukses Update Data!');
    }

    public function golonganDelete(Request $request)
    {
      $golongan = Golongan::find($request->id);
      $golongan->active = 'false';
      $golongan->save();

      return redirect()->back()->with('message', 'Sukses Hapus Data!');
    }

    public function subGolonganList(Request $request)
    {
      $subGolongans = SubGolongan::where('active', 'true')->get();

      $data = [
        'subGolongans' => $subGolongans
      ];

      return view('master.subGolongan.index', $data);
    }

    public function subGolonganCreate(Request $request)
    {
      return view('master.subGolongan.create');
    }

    public function subGolonganInsert(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $subGolongan = SubGolongan::create([
        'kode' => $request->input('kode'),
        'nama' => $request->input('nama'),
        'active' => 'true'
      ]);

      return redirect()->back()->with('message', 'Sukses Input Data!');
    }

    public function subGolonganEdit(Request $request)
    {
      $subGolongan = SubGolongan::find($request->id);

      $data = [
        'subGolongan' => $subGolongan,
      ];

      return view('master.subGolongan.edit', $data);
    }

    public function subGolonganUpdate(Request $request)
    {
      $rules = [
        'kode' => 'required',
        'nama' => 'required'
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $subGolongan = SubGolongan::find($request->id);
      $subGolongan->kode = $request->input('kode');
      $subGolongan->nama = $request->input('nama');
      $subGolongan->save();

      return redirect()->back()->with('message', 'Sukses Update Data!');
    }

    public function subGolonganDelete(Request $request)
    {
      $subGolongan = SubGolongan::find($request->id);
      $subGolongan->active = 'false';
      $subGolongan->save();

      return redirect()->back()->with('message', 'Sukses Hapus Data!');
    }

    public function lokasiList(Request $request)
    {
      $lokasis = Lokasi::where('active', 'true')->get();

      $data = [
        'lokasis' => $lokasis
      ];

      return view('master.lokasi.index', $data);
    }

    public function lokasiCreate(Request $request)
    {
      // next update
    }

    public function lokasiEdit(Request $request)
    {
      // next update
    }

    public function lokasiUpdate(Request $request)
    {
      // next update
    }

    public function lokasiDelete(Request $request)
    {
      // next update
    }

    public function picList(Request $request)
    {
      $pics = Pic::where('active', 'true')->get();

      $data = [
        'pics' => $pics
      ];

      return view('master.pic.index', $data);
    }

    public function picCreate(Request $request)
    {
      // next update
    }

    public function picEdit(Request $request)
    {
      // next update
    }

    public function picUpdate(Request $request)
    {
      // next update
    }

    public function picDelete(Request $request)
    {
      // next update
    }

    public function supplierList(Request $request)
    {
      $suppliers = Supplier::where('active', 'true')->get();

      $data = [
        'suppliers' => $suppliers
      ];

      return view('master.supplier.index', $data);
    }

    public function supplierCreate(Request $request)
    {
      // next update
    }

    public function supplierEdit(Request $request)
    {
      // next update
    }

    public function supplierUpdate(Request $request)
    {
      // next update
    }

    public function supplierDelete(Request $request)
    {
      // next update
    }

    public function tipeAsetList(Request $request)
    {
      $tipeAsets = TipeAset::where('active', 'true')->get();

      $data = [
        'tipeAsets' => $tipeAsets
      ];

      return view('master.tipeAset.index', $data);
    }

    public function tipeAsetCreate(Request $request)
    {
      // next update
    }

    public function tipeAsetEdit(Request $request)
    {
      // next update
    }

    public function tipeAsetUpdate(Request $request)
    {
      // next update
    }

    public function tipeAsetDelete(Request $request)
    {
      // next update
    }

    public function userList(Request $request)
    {
      $users = User::all();

      $data = [
        'users' => $users
      ];

      return view('user.index', $data);
    }

    public function createUser(Request $request)
    {
      return view('user.create');
    }

    public function insertUser(Request $request)
    {
      $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'nik' => 'required',
        'jabatan' => 'required',
        'password' => 'required|confirmed',
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      User::create([
          'name' => $request->name,
          'email' => $request->email,
          'nik' => $request->nik,
          'jabatan' => $request->jabatan,
          'password' => Hash::make($request->password),
      ]);

      return redirect()->back()->with('message', 'Sukses Input Data!');
    }

    public function editUser(Request $request)
    {
      $user = User::find($request->id);

      $data = [
        'user' => $user
      ];
      return view('user.edit', $data);
    }

    public function updateUser(Request $request)
    {
      $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'nik' => 'required',
        'jabatan' => 'required',
        'password' => 'confirmed',
      ];

      $messages = [
        'required' => ':attribute belum di isi',
      ];

      $this->validate($request, $rules, $messages);

      $userUpdate = User::find($request->id);
      $userUpdate->name = $request->name;
      $userUpdate->email = $request->email;
      $userUpdate->nik = $request->nik;
      $userUpdate->jabatan = $request->jabatan;
      if (isset($request->password)) {
        $userUpdate->password = Hash::make($request->password);
      }
      $userUpdate->save();

      return redirect()->back()->with('message', 'Sukses Update Data!');
    }

    public function deleteUser(Request $request)
    {
      $user = User::find($request->id);
      $user->delete();

      return redirect()->back()->with('message', 'Sukses Hapus Data!');
    }
}
