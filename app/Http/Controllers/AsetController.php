<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Input;
use Image;
use File;
use Log;
use App\Department;
use App\Golongan;
use App\SubGolongan;
use App\Lokasi;
use App\Pic;
use App\Supplier;
use App\TipeAset;
use App\Assets;
use App\User;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use peal\barcodegenerator\Facades\BarCode;

class AsetController extends Controller
{
  public function list(Request $request)
  {
    $asets = Assets::with('department')->where('active', 'true')->where('status', 'input')->get();

    $data = [
      'asets' => $asets
    ];

    return view('aset.index', $data);
  }

  public function create(Request $request)
  {
    $departments = Department::where('active', 'true')->get();
    $golongans = Golongan::where('active', 'true')->get();
    $subGolongans = SubGolongan::where('active', 'true')->get();
    $lokasis = Lokasi::where('active', 'true')->get();
    $pics = Pic::where('active', 'true')->get();
    $suppliers = Supplier::where('active', 'true')->get();
    $tipeAsets = TipeAset::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'golongans' => $golongans,
      'subGolongans' => $subGolongans,
      'lokasis' => $lokasis,
      'pics' => $pics,
      'suppliers' => $suppliers,
      'tipeAsets' => $tipeAsets,
    ];

    return view('aset.create', $data);
  }

  public function insert(Request $request)
  {
    // return $request->input();
    $department = Department::where('kode', $request->input('department'))->first();
    $golongan = Golongan::where('kode', $request->input('golongan'))->first();
    $subGolongan = SubGolongan::where('kode', $request->input('sub_golongan'))->first();
    $asets = Assets::count();
    $pendataanKe = str_pad($asets+1, 4, '0', STR_PAD_LEFT);
    $kodeAset = $department->initial.$department->kode.$golongan->kode.$subGolongan->kode.$pendataanKe;
    // return $kodeAset;
    $barcode = [
      'text' => $kodeAset,
      'size' => 100,
      'orientation' => 'horizontal',
      'code_type' => 'code128',
      'print' => true,
      'sizefactor' => 1,
      'filename' => $kodeAset.'.jpeg',
      'filepath' => 'prdbarcode'
    ];

    $barcontent = BarCode::barcodeFactory()->renderBarcode(
      $text=$kodeAset,
      $size=100,
      $orientation='horizontal',
      $code_type='code128', // code_type : code128,code39,code128b,code128a,code25,codabar
      $print=true,
      $sizefactor=1,
      $filename = $kodeAset.'.jpeg',
      $filepath = 'prdbarcode'
      )->filename($kodeAset.'.jpeg');

      if ($request->hasFile('foto')) {
        $image      = $request->file('foto');
        $fileName   = $kodeAset . '.' . $image->getClientOriginalExtension();

        $img = Image::make($image->getRealPath());

        $img->stream(); // <-- Key point
        //dd();
        Storage::disk('public')->put('aset/foto'.'/'.$fileName, $img, 'public');
        // return $img;
        // return $contents = Storage::disk('public')->get('aset/foto/'.$fileName);
        $fotoUrl = url('/uploads/aset/foto').'/'.$fileName;
      }
    $rules = [
      'nama' => 'required',
      'tahun_pembelian' => 'required|numeric',
      'merek' => 'required',
      'tipe' => 'required',
      'ukuran' => 'required',
      'harga' => 'required|numeric',
      'jumlah' => 'required|numeric',
      'perkiraan_umur_ekonomis' => 'required|numeric',
      'foto' => 'required'
    ];

    $messages = [
      'required' => ':attribute belum di isi',
    ];

    $this->validate($request, $rules, $messages);

    $tahunBerjalan = Carbon::now()->year - $request->input('tahun_pembelian');

    $createAset = Assets::create([
      'kode_aset' => $kodeAset,
      'kode_dept' => $request->input('department'),
      'kode_golongan' => $request->input('golongan'),
      'kode_subgolongan' => $request->input('sub_golongan'),
      'pendataan_ke' => $pendataanKe,
      'dic' => $department->initial,
      'nama_aset' => $request->input('nama'),
      'tahun_pembelian' => $request->input('tahun_pembelian'),
      'tahun_berjalan' => $tahunBerjalan,
      'merek' => $request->input('merek'),
      'tipe' => $request->input('tipe'),
      'ukuran' => $request->input('ukuran'),
      'harga' => $request->input('harga'),
      'jumlah' => $request->input('jumlah'),
      'spesifikasi' => $request->input('spesifikasi'),
      'perkiraan_umur_ekonomis' => $request->input('perkiraan_umur_ekonomis'),
      'identitas_komputer' => $request->input('identitas_komputer'),
      'no_mesin' => $request->input('no_mesin'),
      'no_rangka' => $request->input('no_rangka'),
      'warna' => $request->input('warna'),
      'no_bpkb' => $request->input('no_bpkb'),
      'no_polisi' => $request->input('no_polisi'),
      'kepemilikan_atas_nama' => $request->input('kepemilikan_atas_nama'),
      'model_pembuatan' => $request->input('model_pembuatan'),
      'tipe_aset' => $request->input('tipe_aset'),
      'pic' => $request->input('pic'),
      'supplier' => $request->input('supplier'),
      'fungsi' => $request->input('fungsi'),
      'lokasi' => $request->input('department'),
      'barcode' => $barcontent,
      'foto' => $fotoUrl,
      'status' => 'input',
      'active' => 'true',
    ]);

    return redirect()->back()->with('message', 'Sukses Input Data!');

  }

  public function detail(Request $request)
  {
    $aset = Assets::with('department', 'golongan', 'subGolongan')->where('id', $request->id)->first();
    $data = [
      'aset' => $aset
    ];
    return view('aset.detail', $data);
  }

  public function edit(Request $request)
  {
    $aset = Assets::find($request->id);
    $departments = Department::where('active', 'true')->get();
    $golongans = Golongan::where('active', 'true')->get();
    $subGolongans = SubGolongan::where('active', 'true')->get();
    $lokasis = Lokasi::where('active', 'true')->get();
    $pics = Pic::where('active', 'true')->get();
    $suppliers = Supplier::where('active', 'true')->get();
    $tipeAsets = TipeAset::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'golongans' => $golongans,
      'subGolongans' => $subGolongans,
      'lokasis' => $lokasis,
      'pics' => $pics,
      'suppliers' => $suppliers,
      'tipeAsets' => $tipeAsets,
      'aset' => $aset
    ];

    return view('aset.edit', $data);
  }

  public function update(Request $request)
  {
    // return $request->input();
    $department = Department::where('kode', $request->input('department'))->first();
    $golongan = Golongan::where('kode', $request->input('golongan'))->first();
    $subGolongan = SubGolongan::where('kode', $request->input('sub_golongan'))->first();
    $aset = Assets::find($request->id);

    if ($request->hasFile('foto')) {
      $image      = $request->file('foto');
      $fileName   = $aset->kode_aset . '.' . $image->getClientOriginalExtension();

      $img = Image::make($image->getRealPath());

      $img->stream(); // <-- Key point
      //dd();
      Storage::disk('public')->put('aset/foto'.'/'.$fileName, $img, 'public');
      // return $img;
      // return $contents = Storage::disk('public')->get('aset/foto/'.$fileName);
      $fotoUrl = url('/uploads/aset/foto').'/'.$fileName;
    }

    $rules = [
      'nama' => 'required',
      'tahun_pembelian' => 'required|numeric',
      'merek' => 'required',
      'tipe' => 'required',
      'ukuran' => 'required',
      'harga' => 'required|numeric',
      'jumlah' => 'required|numeric',
      'perkiraan_umur_ekonomis' => 'required|numeric',
      'foto' => 'required'
    ];

    $messages = [
      'required' => ':attribute belum di isi',
    ];

    $this->validate($request, $rules, $messages);

    $tahunBerjalan = Carbon::now()->year - $request->input('tahun_pembelian');

    $updateAset = Assets::find($request->id);

    $updateAset->kode_dept = $request->input("department");
    $updateAset->kode_golongan = $request->input("golongan");
    $updateAset->kode_subgolongan = $request->input("sub_golongan");
    $updateAset->dic = $department->initial;
    $updateAset->nama_aset = $request->input("nama");
    $updateAset->tahun_pembelian = $request->input("tahun_pembelian");
    $updateAset->tahun_berjalan = $tahunBerjalan;
    $updateAset->merek = $request->input("merek");
    $updateAset->tipe = $request->input("tipe");
    $updateAset->ukuran = $request->input("ukuran");
    $updateAset->harga = $request->input("harga");
    $updateAset->jumlah = $request->input("jumlah");
    $updateAset->spesifikasi = $request->input("spesifikasi");
    $updateAset->perkiraan_umur_ekonomis = $request->input("perkiraan_umur_ekonomis");
    $updateAset->identitas_komputer = $request->input("identitas_komputer");
    $updateAset->no_mesin = $request->input("no_mesin");
    $updateAset->no_rangka = $request->input("no_rangka");
    $updateAset->warna = $request->input("warna");
    $updateAset->no_bpkb = $request->input("no_bpkb");
    $updateAset->no_polisi = $request->input("no_polisi");
    $updateAset->kepemilikan_atas_nama = $request->input("kepemilikan_atas_nama");
    $updateAset->model_pembuatan = $request->input("model_pembuatan");
    $updateAset->tipe_aset = $request->input("tipe_aset");
    $updateAset->pic = $request->input("pic");
    $updateAset->supplier = $request->input("supplier");
    $updateAset->fungsi = $request->input("fungsi");
    $updateAset->lokasi = $request->input("department");
    $updateAset->foto = $fotoUrl;
    $updateAset->status = "input";
    $updateAset->active = "true";
    $updateAset->save();

    return redirect()->back()->with('message', 'Sukses Update Data!');

  }

  public function delete(Request $request)
  {
    $user = User::where('email', '=', $request->email)->whereIn('jabatan', ['supervisor'])->first();
    if ($user) {
      if (Hash::check($request->password, $user->password)) {
        $aset = Assets::find($request->id);
        $aset->active = 'false';
        $aset->save();

        return redirect()->back()->with('message', 'Sukses Hapus Aset!');
      } else {
        return redirect()->back()->with('error', 'Anda tidak memiliki akses!');
      }
    } else {
      return redirect()->back()->with('error', 'Anda tidak memiliki akses!');
    }
  }

  public function pemusnahanAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'input')->get();

    $data = [
      'asets' => $asets
    ];

    return view('pemusnahan_aset.pemusnahan_aset', $data);
  }

  public function penurunanNilaiAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->get();
    $departments = Department::where('active', 'true')->get();
    $golongans = Golongan::where('active', 'true')->get();
    $subGolongans = SubGolongan::where('active', 'true')->get();
    $lokasis = Lokasi::where('active', 'true')->get();
    $pics = Pic::where('active', 'true')->get();
    $suppliers = Supplier::where('active', 'true')->get();
    $tipeAsets = TipeAset::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'golongans' => $golongans,
      'subGolongans' => $subGolongans,
      'lokasis' => $lokasis,
      'pics' => $pics,
      'suppliers' => $suppliers,
      'tipeAsets' => $tipeAsets,
      'asets' => $asets
    ];

    return view('penilaian_aset.penurunan_nilai_aset', $data);
  }

  public function perbaikanAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->get();
    $departments = Department::where('active', 'true')->get();
    $golongans = Golongan::where('active', 'true')->get();
    $subGolongans = SubGolongan::where('active', 'true')->get();
    $lokasis = Lokasi::where('active', 'true')->get();
    $pics = Pic::where('active', 'true')->get();
    $suppliers = Supplier::where('active', 'true')->get();
    $tipeAsets = TipeAset::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'golongans' => $golongans,
      'subGolongans' => $subGolongans,
      'lokasis' => $lokasis,
      'pics' => $pics,
      'suppliers' => $suppliers,
      'tipeAsets' => $tipeAsets,
      'asets' => $asets
    ];

    return view('pemeriksaan_aset.perbaikan_aset', $data);
  }

  public function asetRusak(Request $request)
  {
    $asets = Assets::where('active', 'true')->get();
    $departments = Department::where('active', 'true')->get();
    $golongans = Golongan::where('active', 'true')->get();
    $subGolongans = SubGolongan::where('active', 'true')->get();
    $lokasis = Lokasi::where('active', 'true')->get();
    $pics = Pic::where('active', 'true')->get();
    $suppliers = Supplier::where('active', 'true')->get();
    $tipeAsets = TipeAset::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'golongans' => $golongans,
      'subGolongans' => $subGolongans,
      'lokasis' => $lokasis,
      'pics' => $pics,
      'suppliers' => $suppliers,
      'tipeAsets' => $tipeAsets,
      'asets' => $asets
    ];

    return view('pemeriksaan_aset.aset_rusak', $data);
  }

  public function pindahLokasiAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'input')->get();
    $departments = Department::where('active', 'true')->get();

    $data = [
      'departments' => $departments,
      'asets' => $asets
    ];

    return view('pengalihan_aset.pindah_lokasi_aset', $data);
  }

  public function pindahAset(Request $request)
  {
    $pindah = Assets::find($request->id);
    $pindah->kode_dept = $request->department;
    $pindah->save();

    return redirect()->back()->with('message', 'Aset dipindahkan!');
  }

  public function penjualanAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'input')->get();

    $data = [
      'asets' => $asets
    ];

    return view('pengalihan_aset.penjualan_aset', $data);
  }

  public function jualAset(Request $request)
  {
    $request->session()->put('id_asset', $request->id);

    $rules = [
      'nama_pembeli' => 'required',
      'perusahaan_pembeli' => 'required',
      'alamat_pembeli' => 'required',
      'telp_pembeli' => 'required',
      'nilai_jual' => 'required',
      'keterangan_jual' => 'required'
    ];

    $messages = [
      'required' => ':attribute belum di isi',
    ];

    $this->validate($request, $rules, $messages);

    $jualAset = Assets::find($request->id);
    $jualAset->status = 'terjual';
    $jualAset->nama_pembeli = $request->nama_pembeli;
    $jualAset->perusahaan_pembeli = $request->perusahaan_pembeli;
    $jualAset->alamat_pembeli = $request->alamat_pembeli;
    $jualAset->telp_pembeli = $request->telp_pembeli;
    $jualAset->nilai_jual = $request->nilai_jual;
    $jualAset->keterangan_jual = $request->keterangan_jual;
    $jualAset->save();

    return redirect()->back()->with('message', 'Aset terjual!');
  }

  public function dataAsetRusak(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'rusak')->get();

    $data = [
      'asets' => $asets
    ];

    return view('laporan_aset.data_aset_rusak', $data);
  }

  public function dataAsetTerjual(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'terjual')->get();

    $data = [
      'asets' => $asets
    ];

    return view('laporan_aset.data_aset_terjual', $data);
  }

  public function dataAsetTermusnahkan(Request $request)
  {
    $asets = Assets::where('active', 'false')->get();

    $data = [
      'asets' => $asets
    ];

    return view('laporan_aset.data_aset_termusnahkan', $data);
  }

  public function dataAsetBerdasarkanKategori(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'input')->get();

    $data = [
      'asets' => $asets
    ];

    return view('laporan_aset.data_aset_berdasarkan_kategori', $data);
  }

  public function lokasiAset(Request $request)
  {
    $asets = Assets::where('active', 'true')->where('status', 'input')->get();

    $data = [
      'asets' => $asets
    ];

    return view('laporan_aset.lokasi_aset', $data);
  }
}
