<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
	protected $guarded = ['id'];

  public function department()
  {
      return $this->hasOne('App\Department', 'kode', 'kode_dept');
  }
	public function golongan()
  {
      return $this->hasOne('App\Golongan', 'kode', 'kode_golongan');
  }
	public function subGolongan()
  {
      return $this->hasOne('App\SubGolongan', 'kode', 'kode_subgolongan');
  }
}
