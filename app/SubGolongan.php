<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubGolongan extends Model
{
  protected $guarded = ['id'];
  
  public function aset()
  {
    return $this->hasMany('App\Assets', 'kode_subgolongan', 'kode');
  }
}
