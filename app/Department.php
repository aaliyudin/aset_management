<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  
  protected $guarded = ['id'];

  public function aset()
  {
    return $this->hasMany('App\Assets', 'kode_dept', 'kode');
  }
}
