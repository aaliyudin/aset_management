<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
  protected $guarded = ['id'];

  public function aset()
  {
    return $this->hasMany('App\Assets', 'kode_golongan', 'kode');
  }
}
