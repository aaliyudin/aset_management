@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Pemusnahan Aset</h1>
@stop

@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
        <a href="#" class="btn btn-danger hapus_aset" data-toggle="modal" data-id="{{$aset->id}}" data-target="#modal-default"><i class="fas fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>

@if(session()->has('message'))
<div class="modal fade show" id="modal-sm" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aset</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{ session()->get('message') }}</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endif

<div class="modal fade" id="modal-default" aria-hidden="true">
  <div class="modal-dialog">
    <form id="hapus_aset" action="{{url('aset/delete')}}" method="post">
      @csrf
      <input type="hidden" class="delete_data_aset" name="id" value="{{ Session::get('id_asset')}}">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus Aset</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          @if(session()->has('error'))
          <div class="alert alert-danger">
            {{ session()->get('error') }}
          </div>
          @endif
          @csrf
          <div class="form-group">
            <label for="ukuran">Silahkan masukan email suppervisor!</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="ukuran">Silahkan masukan password!</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-small">Hapus Aset</a>
        </div>
      </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade show" id="modal-small" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary tetap_hapus" data-toggle="modal" data-target="#modal-small">Hapus Aset</a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
@if(session()->has('error'))
<script type="text/javascript">
$(document).ready(function() {
  $('#modal-default').modal('show');
});
</script>
@endif

<script>
$(document).ready(function() {
  $('#example').DataTable();

  $('#modal-sm').modal('show');

  $('.hapus_aset').on('click', function(){
    var id = $(this).attr('data-id');
    $('.delete_data_aset').val(id);
  });
  $('.tetap_hapus').on('click', function(){
    $( "#hapus_aset" ).submit();
  });
});
</script>
@stop
