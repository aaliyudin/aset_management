@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Penurunan Nilai Aset</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>{{date("Y") - $aset->tahun_pembelian}}</td>
      <td>{{number_format($aset->harga)}}</td>
      <?php
        $nResidu = $aset->harga / $aset->perkiraan_umur_ekonomis;
        $nDefresiasi = $aset->harga - $nResidu;
        $nDefresiasi = $nDefresiasi / $aset->perkiraan_umur_ekonomis;
      ?>
      <td>{{number_format($nResidu)}}</td>
      <td>{{number_format($nDefresiasi)}}</td>
      <td>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();
});
</script>
@stop
