@extends('adminlte::page')

@section('title', 'Pengadaan Aset')

@section('content_header')
<h1>Tambah User</h1>
@stop

@section('content')
<div class="card card-primary">
  <div class="card">
      <div class="card-header">
        <h3 class="card-title">User</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">

        @if(session()->has('message'))
        <div class="alert alert-success">
          {{ session()->get('message') }}
        </div>
        @endif
        <form action="{{ url('user/insert') }}" method="post">
            {{ csrf_field() }}

            {{-- Name field --}}
            <div class="input-group mb-3">
                <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                       value="{{ old('name') }}" placeholder="Nama" autofocus>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </div>
                @endif
            </div>

            {{-- Email field --}}
            <div class="input-group mb-3">
                <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                       value="{{ old('email') }}" placeholder="Email">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif
            </div>

            <div class="input-group mb-3">
                <input type="text" name="nik"
                       class="form-control {{ $errors->has('nik') ? 'is-invalid' : '' }}"
                       placeholder="NIK">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('nik'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('nik') }}</strong>
                    </div>
                @endif
            </div>

            <div class="input-group mb-3">
                <select class="form-control {{ $errors->has('jabatan') ? 'is-invalid' : '' }}" name="jabatan">
                  <option value="admin">Admin</option>
                  <option value="supervisor">Supervisor</option>
                  <option value="it-support">IT Support</option>
                </select>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('jabatan'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('jabatan') }}</strong>
                    </div>
                @endif
            </div>

            {{-- Password field --}}
            <div class="input-group mb-3">
                <input type="password" name="password"
                       class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                       placeholder="Password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </div>
                @endif
            </div>

            {{-- Confirm password field --}}
            <div class="input-group mb-3">
                <input type="password" name="password_confirmation"
                       class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                       placeholder="Ulangi Password">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                    </div>
                </div>
                @if($errors->has('password_confirmation'))
                    <div class="invalid-feedback">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </div>
                @endif
            </div>

            {{-- Register button --}}
            <button type="submit" class="btn btn-primary {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }} float-right">
                Tambah
            </button>
            <a href="{{url('user/list')}}" class="btn btn-default">Cancel</a>

        </form>
      </div>
      <!-- /.card-body -->
    </div>

</div>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
<style media="screen">
  .hidden{
    display: none;
  }
</style>
@stop

@section('js')
<script>
  $(document).ready(function(){
    $('#tipeAset').on('change', function(){
      var tipeAset = $('#tipeAset').val().toLowerCase();
      var tipe_aset = tipeAset.replace(" ", "_");
      $('.tipe_aset').addClass('hidden');
      $('.'+tipe_aset).removeClass('hidden');
    });
  });
</script>
@stop
