@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Data User</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Email</th>
      <th>Nama</th>
      <th>NIK</th>
      <th>Jabatan</th>
      <th>Terakhir Login</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($users as $key => $user)
    <tr>
      <td>{{$user->email}}</td>
      <td>{{$user->name}}</td>
      <td>{{$user->nik}}</td>
      <td>{{$user->jabatan}}</td>
      <td>{{$user->last_login}}</td>
      <td>
        <a href="{{url('user/edit?id=')}}{{$user->id}}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
        <button type="button" class="btn btn-danger hapus_user" data-toggle="modal" data-url="{{url('user/delete?id=')}}{{$user->id}}" data-target="#modal-default"><i class="fas fa-trash"></i></button>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Email</th>
      <th>Nama</th>
      <th>NIK</th>
      <th>Jabatan</th>
      <th>Terakhir Login</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>

<div class="modal fade" id="modal-default" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Hapus User</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <a href="" class="btn btn-primary delete_data_user">Hapus Aset</a>
        </div>
      </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@if(session()->has('message'))
<div class="modal fade show" id="modal-sm" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aset</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{ session()->get('message') }}</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endif
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();

  $('#modal-sm').modal('show');

  $('.hapus_user').on('click', function(){
    var url = $(this).attr('data-url');
    $('.delete_data_user').attr('href', url);
  });
});
</script>
@stop
