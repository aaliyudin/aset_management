@extends('adminlte::page')

@section('title', 'Pengadaan Aset')

@section('content_header')
<h1>Pengadaan Aset</h1>
@stop

@section('content')
<div class="card card-primary">
  @if($errors->any())
  {!! implode('', $errors->all('<div style="color:red; margin-left:1.25rem;">:message</div>')) !!}
  @endif
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif
  <form class="" action="{{url('aset/update')}}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$aset->id}}">
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label>Department</label>
            <select name="department" class="form-control">
              @foreach($departments as $department)
              <option value="{{$department->kode}}" {{ $department->kode == $aset->kode_dept ? 'selected' : '' }}>{{$department->nama}} [{{$department->kode}}]</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Golongan</label>
            <select name="golongan" class="form-control">
              @foreach($golongans as $golongan)
              <option value="{{$golongan->kode}}" {{ $golongan->kode == $aset->kode_golongan ? 'selected' : '' }}>{{$golongan->nama}} [{{$golongan->kode}}]</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label>Sub Golongan</label>
            <select name="sub_golongan" class="form-control">
              @foreach($subGolongans as $subGolongan)
              <option value="{{$subGolongan->kode}}" {{ $subGolongan->kode == $aset->kode_subgolongan ? 'selected' : '' }}>{{$subGolongan->nama}} [{{$subGolongan->kode}}]</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="namaAset">Nama Aset</label>
            <input type="text" name="nama" class="form-control" id="namaAset" value="{{$aset->nama_aset}}" placeholder="Masukan Nama Aset">
          </div>
          <div class="form-group">
            <label for="tahunPembelian">Tahun Pembelian</label>
            <input type="number" name="tahun_pembelian" class="form-control" id="tahunPembelian" value="{{$aset->tahun_pembelian}}" placeholder="Tahun Pembelian">
          </div>
          <div class="form-group">
            <label for="merek">Merek</label>
            <input type="text" name="merek" class="form-control" id="merek" value="{{$aset->merek}}" placeholder="Merek">
          </div>
          <div class="form-group">
            <label for="tipe">Tipe</label>
            <input type="text" name="tipe" class="form-control" id="tipe" value="{{$aset->tipe}}" placeholder="Tipe">
          </div>
          <div class="form-group">
            <label for="ukuran">Ukuran</label>
            <input type="text" name="ukuran" class="form-control" id="ukuran" value="{{$aset->ukuran}}" placeholder="Ukuran">
          </div>
          <div class="form-group">
            <label for="harga">Harga</label>
            <input type="number" name="harga" class="form-control" id="harga" value="{{$aset->harga}}" placeholder="Harga">
          </div>
          <div class="form-group">
            <label for="jumlah">Jumlah</label>
            <input type="number" name="jumlah" class="form-control" id="jumlah" value="{{$aset->jumlah}}" placeholder="Jumlah">
          </div>
          <div class="form-group">
            <label for="spesifikasi">Spesifikasi</label>
            <input type="text" name="spesifikasi" class="form-control" id="spesifikasi" value="{{$aset->spesifikasi}}" placeholder="Spesifikasi">
          </div>
          <div class="form-group">
            <label for="perkiraan_umur_ekonomis">Perkiraan Umur Ekonomis</label>
            <input type="number" name="perkiraan_umur_ekonomis" class="form-control" id="perkiraan_umur_ekonomis" value="{{$aset->perkiraan_umur_ekonomis}}" placeholder="Perkiraan Umur Ekonomis">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Tipe Aset</label>
            <select name="tipe_aset" id="tipeAset" class="form-control">
              @foreach($tipeAsets as $tipeAset)
              <option {{ $tipeAset->nama == $aset->tipe_aset ? 'selected' : '' }}>{{$tipeAset->nama}}</option>
              @endforeach
            </select>
          </div>
          <div class="tipe_aset komputer">
            <div class="form-group">
              <label for="identitas_komputer">Identitas Komputer</label>
              <input type="text" name="identitas_komputer" class="form-control" id="identitas_komputer" value="{{$aset->identitas_komputer}}" placeholder="Identitas Komputer">
            </div>
          </div>
          <div class="tipe_aset kendaraan hidden">
            <div class="form-group">
              <label for="no_mesin">Nomor Mesin</label>
              <input type="text" name="no_mesin" class="form-control" id="no_mesin" value="{{$aset->no_mesin}}" placeholder="Nomor Mesin">
            </div>
            <div class="form-group">
              <label for="no_rangka">Nomor Rangka</label>
              <input type="text" name="no_rangka" class="form-control" id="no_rangka" value="{{$aset->no_rangka}}" placeholder="Nomor Rangka">
            </div>
            <div class="form-group">
              <label for="warna">Warna</label>
              <input type="text" name="warna" class="form-control" id="warna" value="{{$aset->warna}}" placeholder="Warna">
            </div>
            <div class="form-group">
              <label for="no_bpkb">Nomor BPKB</label>
              <input type="text" name="no_bpkb" class="form-control" id="no_bpkb" value="{{$aset->no_bpkb}}" placeholder="Nomor BPKB">
            </div>
            <div class="form-group">
              <label for="no_polisi">Nomor POLISI</label>
              <input type="text" name="no_polisi" class="form-control" id="no_polisi" value="{{$aset->no_polisi}}" placeholder="Nomor POLISI">
            </div>
            <div class="form-group">
              <label for="kepemilikan_atas_nama">Kepemilikan Atas Nama</label>
              <input type="text" name="kepemilikan_atas_nama" class="form-control" id="kepemilikan_atas_nama" value="{{$aset->kepemilikan_atas_nama}}" placeholder="Kepemilikan Atas Nama">
            </div>
            <div class="form-group">
              <label for="model_pembuatan">Model Pembuatan</label>
              <input type="text" name="model_pembuatan" class="form-control" id="model_pembuatan" value="{{$aset->model_pembuatan}}" placeholder="Model Pembuatan">
            </div>
            <div class="form-group">
              <label for="pic">PIC</label>
              <input type="text" name="pic" class="form-control" id="pic" value="{{$aset->pic}}" placeholder="PIC">
            </div>
          </div>
          <div class="tipe_aset mesin_produksi hidden">
            <div class="form-group">
              <label for="supplier">Supplier</label>
              <input type="text" name="supplier" class="form-control" id="supplier" value="{{$aset->supplier}}" placeholder="Supplier">
            </div>
            <div class="form-group">
              <label for="fungsi">Fungsi</label>
              <input type="text" name="fungsi" class="form-control" id="fungsi" value="{{$aset->fungsi}}" placeholder="Fungsi">
            </div>
          </div>
          <!-- <div class="form-group hidden">
            <label>Lokasi</label>
            <select name="lokasi" class="form-control">
              @foreach($lokasis as $lokasi)
              <option value="{{$lokasi->id}}">{{$lokasi->nama}} [{{$lokasi->kode}}]</option>
              @endforeach
            </select>
          </div> -->
          <img src="{{asset($aset->foto)}}" width="500" alt="">
          <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" name="foto" accept="image/*" id="foto">
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <a href="{{url('aset/list')}}" class="btn btn-default">Cancel</a>
      <button type="submit" class="btn btn-info float-right">Submit</button>
    </div>
  </form>
</div>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
<style media="screen">
  .hidden{
    display: none;
  }
</style>
@stop

@section('js')
<script>
  $(document).ready(function(){
    $('#tipeAset').on('change', function(){
      var tipeAset = $('#tipeAset').val().toLowerCase();
      var tipe_aset = tipeAset.replace(" ", "_");
      $('.tipe_aset').addClass('hidden');
      $('.'+tipe_aset).removeClass('hidden');
    });
  });
</script>
@stop
