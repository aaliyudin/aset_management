@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Data Aset</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>
        <a href="{{url('aset/edit?id=')}}{{$aset->id}}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>


@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();
});
</script>
@stop
