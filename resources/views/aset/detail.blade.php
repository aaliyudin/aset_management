@extends('adminlte::page')

@section('title', 'Pengadaan Aset')

@section('content_header')
<h1>Aset</h1>
@stop

@section('content')
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Detail</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
          <div class="row">
            <div class="col-12 col-sm-4">
              <div class="info-box bg-light">
                <div class="info-box-content">
                  <span class="info-box-text text-center text-muted">Harga</span>
                  <span class="info-box-number text-center text-muted mb-0">{{number_format($aset->harga)}}</span>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-4">
              <div class="info-box bg-light">
                <div class="info-box-content">
                  <span class="info-box-text text-center text-muted">Tahun Pembelian</span>
                  <span class="info-box-number text-center text-muted mb-0">{{$aset->tahun_pembelian}}</span>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-4">
              <div class="info-box bg-light">
                <div class="info-box-content">
                  <span class="info-box-text text-center text-muted">Kode Aset</span>
                  <span class="info-box-number text-center text-muted mb-0">{{$aset->kode_aset}}</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <h4>{{$aset->nama_aset}}</h4>
              <img src="{{$aset->foto}}" width="680" alt="">
            </div>
          </div>
        </div>
        <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
          <h3 class="text-muted">{{$aset->nama_aset}}</h3>
          <br>
          <div class="text-muted">
            <p class="text-sm">Department
              <b class="d-block">{{$aset->department->nama}}</b>
            </p>
            <p class="text-sm">Golongan
              <b class="d-block">{{$aset->golongan->nama}}</b>
            </p>
            <p class="text-sm">Sub Golongan
              <b class="d-block">{{$aset->subGolongan->nama}}</b>
            </p>
            <p class="text-sm">Tahun Pembelian
              <b class="d-block">{{$aset->tahun_pembelian}}</b>
            </p>
            <p class="text-sm">Merek
              <b class="d-block">{{$aset->merek}}</b>
            </p>
            <p class="text-sm">Tipe
              <b class="d-block">{{$aset->tipe}}</b>
            </p>
            <p class="text-sm">Ukuran
              <b class="d-block">{{$aset->ukuran}}</b>
            </p>
            <p class="text-sm">Spesifikasi
              <b class="d-block">{{$aset->spesifikasi}}</b>
            </p>
            @if(isset($aset->identitas_komputer))
            <p class="text-sm">Identitas Komputer
              <b class="d-block">{{$aset->identitas_komputer}}</b>
            </p>
            @endif
            @if(isset($aset->no_mesin))
            <p class="text-sm">Nomor Mesin
              <b class="d-block">{{$aset->no_mesin}}</b>
            </p>
            @endif
            @if(isset($aset->no_rangka))
            <p class="text-sm">Nomor Rangka
              <b class="d-block">{{$aset->no_rangka}}</b>
            </p>
            @endif
            @if(isset($aset->warna))
            <p class="text-sm">Warna
              <b class="d-block">{{$aset->warna}}</b>
            </p>
            @endif
            @if(isset($aset->no_bpkb))
            <p class="text-sm">Nomor BPKB
              <b class="d-block">{{$aset->no_bpkb}}</b>
            </p>
            @endif
            @if(isset($aset->no_polisi))
            <p class="text-sm">Nomor Polisi
              <b class="d-block">{{$aset->no_polisi}}</b>
            </p>
            @endif
            @if(isset($aset->kepemilikan_atas_nama))
            <p class="text-sm">Kepemilikan Atas Nama
              <b class="d-block">{{$aset->kepemilikan_atas_nama}}</b>
            </p>
            @endif
            @if(isset($aset->model_pembuatan))
            <p class="text-sm">Model Pembuatan
              <b class="d-block">{{$aset->model_pembuatan}}</b>
            </p>
            @endif
            @if(isset($aset->pic))
            <p class="text-sm">PIC
              <b class="d-block">{{$aset->pic}}</b>
            </p>
            @endif
            @if(isset($aset->supplier))
            <p class="text-sm">Supplier
              <b class="d-block">{{$aset->supplier}}</b>
            </p>
            @endif
            @if(isset($aset->fungsi))
            <p class="text-sm">Fungsi
              <b class="d-block">{{$aset->fungsi}}</b>
            </p>
            @endif
          </div>

          <h5 class="mt-5 text-muted">Aset Files</h5>
          <ul class="list-unstyled">
            <li>
              <a href="#" class="btn-link text-secondary" data-toggle="modal" data-target="#modal-default"><i class="far fa-fw fa-image "></i> BarCode</a>
            </li>
          </ul>
          <div class="text-center mt-5 mb-3 hidden">
            <a href="#" class="btn btn-sm btn-primary">Jual</a>
            <a href="#" class="btn btn-sm btn-warning">Report contact</a>
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <div class="modal fade" id="modal-default" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">BarCode</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <img src="{{url('/')}}/{{$aset->barcode}}" alt="">
          <button onclick="printImg('{{url('/')}}/{{$aset->barcode}}')">Print</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

</section>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
<style media="screen">
  .hidden{
    display: none;
  }
</style>
@stop

@section('js')
<script>
function printImg(url) {
  var win = window.open('');
  win.document.write('<img src="' + url + '" onload="window.print();window.close()" />');
  win.focus();
}
  $(document).ready(function(){

    $('#tipeAset').on('change', function(){
      var tipeAset = $('#tipeAset').val().toLowerCase();
      var tipe_aset = tipeAset.replace(" ", "_");
      $('.tipe_aset').addClass('hidden');
      $('.'+tipe_aset).removeClass('hidden');
    });
  });
</script>
@stop
