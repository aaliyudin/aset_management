@extends('adminlte::page')

@section('title', 'Master')

@section('content_header')
<h1>PIC</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>Nama</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($pics as $key => $pic)
    <tr>
      <td>{{$pic->id}}</td>
      <td>{{$pic->nama}}</td>
      <td>{{$pic->active}}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>ID</th>
      <th>Nama</th>
      <th>Status</th>
    </tr>
  </tfoot>
</table>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();
});
</script>
@stop
