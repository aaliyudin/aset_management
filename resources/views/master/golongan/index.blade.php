@extends('adminlte::page')

@section('title', 'Master')

@section('content_header')
<h1>Golongan</h1>
@stop

@section('content')
@if($errors->any())
{!! implode('', $errors->all('<div style="color:red; margin-left:1.25rem;">:message</div>')) !!}
@endif
@if(session()->has('message'))
<div class="alert alert-success">
  {{ session()->get('message') }}
</div>
@endif
<a href="{{url('master/golongan/create')}}" class="btn btn-primary float-right">Tambah</a>
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>Kode</th>
      <th>Nama</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    @foreach($golongans as $key => $golongan)
    <tr>
      <td>{{$golongan->id}}</td>
      <td>{{$golongan->kode}}</td>
      <td>{{$golongan->nama}}</td>
      <td>
        <a href="{{url('master/golongan/edit?id=')}}{{$golongan->id}}" class="btn btn-success"><i class="fas fa-pencil-alt"></i></a>
        <a href="#" class="btn btn-danger hapus_data" data-toggle="modal" data-id="{{url('master/golongan/delete?id=')}}{{$golongan->id}}" data-target="#modal-small"><i class="fas fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>ID</th>
      <th>Kode</th>
      <th>Nama</th>
      <th>Status</th>
    </tr>
  </tfoot>
</table>

<div class="modal fade show" id="modal-small" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin?</p>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        <a href="" class="btn btn-primary delete_data">Hapus</a>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();

  $('.hapus_data').on('click', function(){
    var url = $(this).attr('data-id');
    $('.delete_data').attr('href', url);
  });

  $('#example tbody').on('click', '.hapus_data', function () {
    var url = $(this).attr('data-id');
    $('.delete_data').attr('href', url);
  });
});
</script>
@stop
