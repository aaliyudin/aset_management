@extends('adminlte::page')

@section('title', 'Golongan')

@section('content_header')
<h1>Golongan</h1>
@stop

@section('content')
<div class="card card-primary">
  @if($errors->any())
  {!! implode('', $errors->all('<div style="color:red; margin-left:1.25rem;">:message</div>')) !!}
  @endif
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif
  <form class="" action="{{url('master/golongan/insert')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="namaAset">Kode Golongan</label>
            <input type="text" name="kode" class="form-control" id="kode" placeholder="Kode">
          </div>
          <div class="form-group">
            <label for="merek">Nama</label>
            <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama">
          </div>
        </div>
      </div>
    </div>
    <div class="card-footer">
      <a href="{{url('master/golongan')}}" class="btn btn-default">Cancel</a>
      <button type="submit" class="btn btn-info float-right">Submit</button>
    </div>
  </form>
</div>
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
<style media="screen">
  .hidden{
    display: none;
  }
</style>
@stop

@section('js')
<script>
  $(document).ready(function(){
    $('#tipeAset').on('change', function(){
      var tipeAset = $('#tipeAset').val().toLowerCase();
      var tipe_aset = tipeAset.replace(" ", "_");
      $('.tipe_aset').addClass('hidden');
      $('.'+tipe_aset).removeClass('hidden');
    });
  });
</script>
@stop
