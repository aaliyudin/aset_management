@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Lokasi Aset</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Lokasi</th>
      <th>Kode Aset</th>
      <th>Nama Aset</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->nama_aset}}</td>
      <!-- <td>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
      </td> -->
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Lokasi</th>
      <th>Kode Aset</th>
      <th>Nama Aset</th>
    </tr>
  </tfoot>
</table>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"> </script>
<script>
$(document).ready(function() {
  $('#example').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'print',
        text: 'print',
        title: 'Laporan Lokasi Aset',
        customize: function ( win ) {
          $(win.document.body)
          .css( 'font-size', '10pt' )
          .css( 'text-align', 'center' )
          .prepend(
            '<img src="{{asset("vendor/adminlte/dist/img/logo_murni.jpeg")}}" style="position:absolute; top:20; left:20; opacity:0.5;" />'
          );

          $(win.document.body).find( 'table' )
          .addClass( 'compact' )
          .css( 'font-size', 'inherit' );
        }
      }
    ],
    initComplete: function () {
      this.api().columns().every( function () {
        var column = this;
        var select = $('<select><option value=""></option></select>')
        .appendTo( $(column.footer()).empty() )
        .on( 'change', function () {
          var val = $.fn.dataTable.util.escapeRegex(
            $(this).val()
          );

          column
          .search( val ? '^'+val+'$' : '', true, false )
          .draw();
        } );

        column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
      } );
    }
  });
});
</script>
@stop
