@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Data Aset Rusak</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>{{date("Y") - $aset->tahun_pembelian}}</td>
      <td>{{number_format($aset->harga)}}</td>
      <?php
        $nResidu = $aset->harga / $aset->perkiraan_umur_ekonomis;
        $nDefresiasi = $aset->harga - $nResidu;
        $nDefresiasi = $nDefresiasi / $aset->perkiraan_umur_ekonomis;
      ?>
      <td>{{number_format($nResidu)}}</td>
      <td>{{number_format($nDefresiasi)}}</td>
      <td>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
        <a href="{{url('aset/delete')}}" class="btn btn-danger"><i class="fas fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>
@stop

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"> </script>
<script>
$(document).ready(function() {
  $('#example').DataTable( {
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'print',
        text: 'print',
        title: 'Datatables example: Customisation of the print view window',
        customize: function ( win ) {
          $(win.document.body)
          .css( 'font-size', '10pt' )
          .prepend(
            '<img src="{{asset("vendor/adminlte/dist/img/logo_murni.jpeg")}}" style="position:absolute; top:0; left:0; opacity:0.5;" />'
          );

          $(win.document.body).find( 'table' )
          .addClass( 'compact' )
          .css( 'font-size', 'inherit' );
        }
      }
    ],
    initComplete: function () {
      this.api().columns().every( function () {
        var column = this;
        var select = $('<select><option value=""></option></select>')
        .appendTo( $(column.footer()).empty() )
        .on( 'change', function () {
          var val = $.fn.dataTable.util.escapeRegex(
            $(this).val()
          );

          column
          .search( val ? '^'+val+'$' : '', true, false )
          .draw();
        } );

        column.data().unique().sort().each( function ( d, j ) {
          select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
      } );
    }
  });
});
</script>
@stop
