@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Penjualan Aset</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>{{date("Y") - $aset->tahun_pembelian}}</td>
      <td>{{number_format($aset->harga)}}</td>
      <?php
      $nResidu = $aset->harga / $aset->perkiraan_umur_ekonomis;
      $nDefresiasi = $aset->harga - $nResidu;
      $nDefresiasi = $nDefresiasi / $aset->perkiraan_umur_ekonomis;
      ?>
      <td>{{number_format($nResidu)}}</td>
      <td>{{number_format($nDefresiasi)}}</td>
      <td>
        <button type="button" class="btn btn-default jual_aset" data-toggle="modal" data-id="{{$aset->id}}" data-target="#modal-default"><i class="fas fa-dollar-sign"></i></button>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Kode Aset</th>
      <th>Department</th>
      <th>Nama Aset</th>
      <th>Tahun Berjalan</th>
      <th>Harga</th>
      <th>Nilai Residu</th>
      <th>Nilai Defresiasi</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>

<div class="modal fade" id="modal-default" aria-hidden="true">
  <div class="modal-dialog">
    <form class="" action="{{url('aset/jual_aset')}}" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Default Modal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          @if($errors->any())
          <div class="alert alert-danger">
          {!! implode('', $errors->all('<p>:message</p>')) !!}
          </div>
          @endif
          @if(session()->has('message'))
          <div class="alert alert-success">
            {{ session()->get('message') }}
          </div>
          @endif
          @csrf
          <input type="hidden" id="id_aset" name="id" value="{{ Session::get('id_asset')}}">
          <div class="form-group">
            <label for="nama_pembeli">Nama Pembeli</label>
            <input type="text" name="nama_pembeli" class="form-control" id="nama_pembeli" placeholder="Nama Pembeli">
          </div>
          <div class="form-group">
            <label for="perusahaan_pembeli">Perusahaan</label>
            <input type="text" name="perusahaan_pembeli" class="form-control" id="perusahaan_pembeli" placeholder="Perusahaan">
          </div>
          <div class="form-group">
            <label for="alamat_pembeli">Alamat</label>
            <input type="text" name="alamat_pembeli" class="form-control" id="alamat_pembeli" placeholder="Alamat">
          </div>
          <div class="form-group">
            <label for="telp_pembeli">Telp</label>
            <input type="text" name="telp_pembeli" class="form-control" id="telp_pembeli" placeholder="Telp">
          </div>
          <div class="form-group">
            <label for="nilai_jual">Nilai Jual</label>
            <input type="number" name="nilai_jual" class="form-control" id="nilai_jual" placeholder="Nilai Jual">
          </div>
          <div class="form-group">
            <label for="keterangan_jual">Keterangan Penjualan</label>
            <input type="text" name="keterangan_jual" class="form-control" id="keterangan_jual" placeholder="Keterangan">
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Jual Aset</button>
        </div>
      </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@if(session()->has('message'))
<div class="modal fade show" id="modal-sm" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aset</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{ session()->get('message') }}</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endif
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')

@if($errors->any())
<script type="text/javascript">
$(document).ready(function() {
  $('#modal-default').modal('show');
});
</script>
@endif

<script>
  $(document).ready(function() {
    $('#example').DataTable();

    $('#modal-sm').modal('show');

    $('.jual_aset').on('click', function(){
      var id = $(this).attr('data-id');
      $('#id_aset').val(id);
    });

  });

</script>
@stop
