@extends('adminlte::page')

@section('title', 'Data Aset')

@section('content_header')
<h1>Pindah Lokasi Aset</h1>
@stop

@section('content')
<table id="example" class="table table-bordered table-hover dataTable" style="width:100%">
  <thead>
    <tr>
      <th>Lokasi</th>
      <th>Kode Aset</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @foreach($asets as $key => $aset)
    <tr>
      <td>{{$aset->department->nama}}</td>
      <td>{{$aset->kode_aset}}</td>
      <td>{{$aset->nama_aset}}</td>
      <td>
        <a href="{{url('aset/detail?id=')}}{{$aset->id}}" class="btn btn-default"><i class="fas fa-info-circle"></i></a>
        <button type="button" class="btn btn-default pindah_aset" data-toggle="modal" data-id="{{$aset->id}}" data-target="#modal-default"><i class="fas fa-map-marker-alt"></i></button>
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th>Lokasi</th>
      <th>Kode Aset</th>
      <th>Nama Aset</th>
      <th>Aksi</th>
    </tr>
  </tfoot>
</table>

<div class="modal fade" id="modal-default" aria-hidden="true">
  <div class="modal-dialog">
    <form class="" action="{{url('aset/pindah_aset')}}" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Pindah Lokasi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          @csrf
          <input type="hidden" id="id_aset" name="id" value="">
          <div class="form-group">
            <label>Lokasi</label>
            <select name="department" class="form-control">
              @foreach($departments as $department)
              <option value="{{$department->kode}}">{{$department->nama}} [{{$department->kode}}]</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Pindah Aset</button>
        </div>
      </div>
    </form>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

@if(session()->has('message'))
<div class="modal fade show" id="modal-sm" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aset</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{{ session()->get('message') }}</p>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endif
@stop

@section('css')
<!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
@stop

@section('js')
<script>
$(document).ready(function() {
  $('#example').DataTable();

  $('#modal-sm').modal('show');

  $('.pindah_aset').on('click', function(){
    var id = $(this).attr('data-id');
    $('#id_aset').val(id);
  });
});
</script>
@stop
