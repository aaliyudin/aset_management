<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email atau password yang anda masukan salah!',
    'throttle' => 'Anda mencoba login terlalu sering, silahkan coba lagi dalam :seconds detik.',
    'email' => 'Email yang anda masukan tidak terdaftar',
    'password' => 'email sudah benar, password anda salah',

];
