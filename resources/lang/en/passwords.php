<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password anda telah di reset!',
    'sent' => 'Link reset password telah terkirim ke email anda!',
    'throttled' => 'Mohon tunggu untuk mencoba lagi.',
    'token' => 'Token anda tidak valid.',
    'user' => "Email yang anda masukan tidak terdaftar.",

];
