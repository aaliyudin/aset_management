<?php

return [

    'full_name'                   => 'Nama',
    'email'                       => 'Email',
    'password'                    => 'Password',
    'retype_password'             => 'Ulangi password',
    'remember_me'                 => 'Ingat Saya',
    'register'                    => 'Daftar',
    'register_a_new_membership'   => 'Daftar Baru',
    'i_forgot_my_password'        => 'Lupa Password',
    'i_already_have_a_membership' => 'Saya sudah memiliki akun',
    'sign_in'                     => 'Login',
    'log_out'                     => 'Logout',
    'toggle_navigation'           => 'Toggle navigasi',
    'login_message'               => 'Silahkan Login',
    'register_message'            => 'Daftar Baru',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Kirim Password Reset Link',
    'verify_message'              => 'Your account needs a verification',
    'verify_email_sent'           => 'A fresh verification link has been sent to your email address.',
    'verify_check_your_email'     => 'Before proceeding, please check your email for a verification link.',
    'verify_if_not_recieved'      => 'If you did not receive the email',
    'verify_request_another'      => 'click here to request another',
    'confirm_password_message'    => 'Silahkan, konfirmasi password untuk melanjutkan.',
];
